﻿import React from "react";

const Component = () => {
    return (
        <div>
            <div className="commentBox">Hello, world! I am a CommentBox.</div>
        </div>
    );
};

export default Component;