import React from "react";
import ReactDom from "react-dom";
import "./css/index.less";
import Component from "./components/Component";

const Index = () => {
    return (
        <>
            <div className= "something" >

                <p>Here is something</p>

                <Component />
                
            </div>
        </>
    )
}

ReactDom.render(
    <Index />,
    document.getElementById("app")
);